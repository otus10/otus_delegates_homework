﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;
namespace Documents
{
    public class DocumentReceiver : IDisposable
    {
        public event DocumentsReadyEventHandler DocumentReady;
        public event TimedOutEventHandler TimedOut;

        public delegate void TimedOutEventHandler(object sender, EventArgs e);
        public delegate void DocumentsReadyEventHandler(object sender, EventArgs e);

        private FileSystemWatcher _fileSystemWatcher;
        private Timer _timer;

        private List<string> _documentTemplateList;
        private List<string> _documentList = new List<string>();

        private bool _isStarted = false;
        public DocumentReceiver(List<string> documentTemplateList)
        {
            _documentTemplateList = documentTemplateList;
        }
        public void Start(string targetDirectory, double waitingInterval)
        {
            if (_isStarted == false)
            {
                CheckTargetDirectory(targetDirectory);
                CheckWaitingInterval(waitingInterval);

                _isStarted = true;
                InitTimer(waitingInterval);
                InitFileSystemWatcher(targetDirectory);
            }
            else 
            {
                Console.WriteLine("DocumentReceiver is already started");
            }
        }

        private void InitTimer(double waitingInterval)
        {
            _timer = new Timer(waitingInterval);
            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
        }

        private void InitFileSystemWatcher(string targetDirectory)
        {
            _fileSystemWatcher = new FileSystemWatcher(targetDirectory);
            _fileSystemWatcher.EnableRaisingEvents = true; ;
            _fileSystemWatcher.NotifyFilter = NotifyFilters.FileName;
            _fileSystemWatcher.Created += OnCreated;
            _fileSystemWatcher.Deleted += OnDeleted;
            _fileSystemWatcher.Renamed += OnRenamed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispose();
            TimedOut?.Invoke(sender, e);
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Created)
            {
                return;
            }
            AddDocument(e.Name);
            CheckDocumentsReady(sender, e);
        }
        private void OnDeleted(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Deleted)
            {
                return;
            }
            RemoveDocument(e.Name);
        }
        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Renamed)
            {
                return;
            }
            RemoveDocument(e.OldName);
            AddDocument(e.Name);
            CheckDocumentsReady(sender, e);

        }

        private void DisposeTimer()
        {
            _timer.Elapsed -= Timer_Elapsed;
            _timer.Stop();
            _timer.Dispose();
        }

        private void DisposeFileSystemWatcher()
        {
            _fileSystemWatcher.Created -= OnCreated;
            _fileSystemWatcher.Deleted -= OnDeleted;
            _fileSystemWatcher.Renamed -= OnRenamed;
            _fileSystemWatcher.Dispose();
        }

        private void CheckDocumentsReady(object sender, EventArgs e)
        {
            if (_documentList.Count == 3)
            {
                Dispose();
                DocumentReady?.Invoke(sender, e);
            }
        }
        private void AddDocument(string documentName)
        {
            if (_documentTemplateList.Contains(documentName))
                _documentList.Add(documentName);

        }
        private void RemoveDocument(string documentName)
        {
            if (_documentTemplateList.Contains(documentName))
                _documentList.Remove(documentName);
        }

        private void CheckTargetDirectory(string path)
        {
            if (!Directory.Exists(path))
                if (string.IsNullOrEmpty(path))
                {
                    string errorMessage = $"targetDirectory parameter is null";
                    Console.WriteLine(errorMessage);
                    throw new ArgumentNullException(errorMessage);
                }
                else
                {
                    string errorMessage = $"Directory not found: {path}";
                    Console.WriteLine(errorMessage);
                    throw new DirectoryNotFoundException($"Directory not found: {errorMessage}");
                }
        }
        private void CheckWaitingInterval(double waitingInterval)
        {
            if (waitingInterval <= 0)
                throw new Exception($"waitingInterval parameter is less or equal than 0: {waitingInterval}");
        }
        public void Dispose()
        {
            if (_isStarted)
            {
                DisposeFileSystemWatcher();
                DisposeTimer();
                _isStarted = false;
            }
        }
    }
}
