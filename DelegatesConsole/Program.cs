﻿using Documents;
using System;
using System.Collections.Generic;

namespace delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> documentTemplateList = new List<string>();
            documentTemplateList.Add("Паспорт.jpg");
            documentTemplateList.Add("Заявление.txt");
            documentTemplateList.Add("Фото.jpg");

            using (DocumentReceiver documentReceiver = new DocumentReceiver(documentTemplateList))
            {

                string directory = @"F:\YandexDisk\MSILocalRepos\otus\delegates\DelegatesConsole\Pictures\recieved\";
                double interval = 100000;

                documentReceiver.DocumentReady += DocumentReceiver_DocumentReady;
                documentReceiver.TimedOut += DocumentReceiver_TimedOut;
                documentReceiver.Start(directory, interval);
                documentReceiver.Start(directory, interval);
                Console.ReadKey();
            }
        }

        private static void DocumentReceiver_TimedOut(object sender, EventArgs e)
        {
            Console.WriteLine(@"Time is over.");
        }

        private static void DocumentReceiver_DocumentReady(object sender, EventArgs e)
        {
            Console.WriteLine(@"All Documents Recieved.");
        }
    }
}
